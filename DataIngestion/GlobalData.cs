﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace DataIngestion
{
    class GlobalData
    {
        public static Dictionary<string, int> factual = new Dictionary<string, int>()
        {
            { "Restaurant", 347 },
            { "AmusementPark", 319 },
            { "Aquarium", 371 },
            { "Zoo", 371 },
            { "Cemetery", 1 },
            { "Church", 55 },
            { "HinduTemple", 56 },
            { "Lodging", 432 },
            { "Mosque", 57 },
            { "Museum", 311 },
            { "NaturalFeature", 119 },
            { "Park", 118 },
            { "PlaceOfWorship", 53 },
            { "PointOfInterest", 1 },
            { "Premise", 1 },
            { "ShoppingMall", 169 },
            { "Spa", 260 },
            { "Store", 442 },
            { "TravelAgency", 430 }
        };

        public static Dictionary<string, string> CountryISO = new Dictionary<string, string>()
        {
            { "malaysia", "MY" },
            { "india", "IN" },
            { "china", "CN" },
            { "france", "FR" }
        };

        //replaces underscores with title casings
        public static string CorrectType(string type)
        {            
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            return textInfo.ToTitleCase(type.Replace('_', ' ').ToLower()).Replace(" ", "");
        }
    }
}
