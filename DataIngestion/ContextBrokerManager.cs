﻿using DataIngestion.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace DataIngestion
{
    class ContextBrokerManager
    {
        public static string apilink = @"http://161.106.242.199:1026";
        public static void CreateEntities<T>(List<T> entities)
        {

            string url = $@"{apilink}/v2/entities";
            foreach (object entity in entities)
            {
                var id = ((IEntity)entity).id;
                var stringPayload = JsonConvert.SerializeObject(entity); //serialize and convert to string

                var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                Console.WriteLine("Creating "+ id);
                using (var httpClient = new HttpClient())
                {
                    var httpResponse = httpClient.PostAsync(url, httpContent).Result; //Run Synchronously to measure actual time taken.
                    string responseContent = "";
                    if (httpResponse.Content != null)
                        responseContent = httpResponse.Content.ReadAsStringAsync().Result;
                    Console.WriteLine(responseContent + " " + httpResponse.StatusCode.ToString());

                }

            }
        }
        public static void CreateEntitiesBulk<T>(List<T> entities)
        {
            string url = $@"{apilink}/v2/op/update";

            //var stringPayload = JsonConvert.SerializeObject(entities); //serialize and convert to string

            var stringPayload = "{\"actionType\": \"APPEND\", \"entities\": " + JsonConvert.SerializeObject(entities) + "}";

            var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");

            using (var httpClient = new HttpClient())
            {
                var httpResponse = httpClient.PostAsync(url, httpContent).Result; //Run Synchronously to measure actual time taken.
                string responseContent = "";
                if (httpResponse.Content != null)
                    responseContent = httpResponse.Content.ReadAsStringAsync().Result;
                Console.WriteLine(responseContent + " " + httpResponse.StatusCode.ToString());

            }


        }
    }
}