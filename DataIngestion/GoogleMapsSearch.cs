﻿using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Linq;
using System.Collections.Generic;
using DataIngestion.Models;
using Newtonsoft.Json;
using System.Threading;
using System.Globalization;

namespace DataIngestion
{
    class GoogleMapsSearch
    {

        public static GooglePlacesResult[] DoSearch(string search, string type)
        {
            string apilink = @"https://maps.googleapis.com/maps/api/place/textsearch/json";
            string url = $@"{apilink}?query={search.Replace(' ', '+')}&key={Secret.googleKey}";
            if (!string.IsNullOrEmpty(type))
                url += $"&type={type}";

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

                GooglePlacesResult[] resarr = new GooglePlacesResult[] { };
                string next_page_token = "";
                for (int i = 0; i < 4; i++)
                {
                    string tmpurl = url;
                    if (!string.IsNullOrEmpty(next_page_token))
                        tmpurl += $"&pagetoken={next_page_token}";

                    string response = httpClient.GetStringAsync(new Uri(tmpurl)).Result;
                    GooglePlaces deserialized = JsonConvert.DeserializeObject<GooglePlaces>(response);
                    resarr = resarr.Union(deserialized.results).ToArray<GooglePlacesResult>();
                    if (next_page_token == null) //If next page does not exists
                        break;
                    next_page_token = deserialized.next_page_token; //Save the next page token

                    Thread.Sleep(2000); //As Google takes some time to generate next page.
                }
                return resarr;
                //var releases = JObject.Parse(response).Children<JProperty>().FirstOrDefault(x => x.Name == "results").Value;
            }
        }

        public static List<PointOfInterest> translateToPOI(GooglePlacesResult[] results)
        {
            List<PointOfInterest> lst = new List<PointOfInterest>();
            Random ran = new Random(3276789);
            foreach (GooglePlacesResult res in results)
            {
                PointOfInterest tmp = new PointOfInterest();
                string[] address = res.formatted_address.Split(',');
                //CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
                //CultureInfo cInfo = cultures.FirstOrDefault(culture => new RegionInfo(culture.LCID).EnglishName == address[address.Length - 1].Trim().ToLower());
                tmp.type = res.types?.Length > 0 ? GlobalData.CorrectType(res.types[0]) : "Generic"; // handle case if api does not return any category
                tmp.id = $"{tmp.type}-{res.place_id}"; //As google id is unique

                try
                {
                    tmp.address = new Address()
                    {
                        type = "StructuredValue",
                        value = new Value()
                        {
                            streetAddress = System.Uri.EscapeDataString(string.Join(",", address.Take(address.Length - 2).ToArray())),
                            addressLocality = address[address.Length - 2],
                            addressCountry = GlobalData.CountryISO[address[address.Length - 1].Trim().ToLower()]
                        }
                    };

                    tmp.category = new Category()
                    {
                        value = new string[1]
                    };
                    if (GlobalData.factual.ContainsKey(tmp.type))
                        tmp.category.value[0] = GlobalData.factual[tmp.type].ToString();
                    else
                        tmp.category.value[0] = "0"; // in case if category does not match
                }
                catch (IndexOutOfRangeException) { } // ignore indexoutofrange issues and continue


                tmp.icon = new Icon()
                {
                    value = res.icon
                };
                tmp.name = new Name()
                {
                    value = System.Uri.EscapeDataString(res.name)
                };

                tmp.description = new Description()
                {
                    value = System.Uri.EscapeDataString(string.Join(",", res.types))
                };

                tmp.location = new LocationF()
                {
                    type = "geo:point",
                    value = $"{res.geometry.location.lat},{res.geometry.location.lng}",
                    metadata = new Metadata()
                    {
                        accuracy = new Accuracy()
                        {
                            type = "float",
                            value = ran.Next(50, 100)
                        }
                    }
                };
                tmp.source = new Source()
                {
                    value = "https://maps.google.com"
                };
                tmp.temperature = new Temperature()
                {
                    type = "float",
                    value = ran.Next(15, 35)
                };
                tmp.inceptionDate = new InceptionDate()
                {
                    type = "DateTime",
                    value = DateTime.UtcNow.AddYears(-ran.Next(10, 200)).ToString("yyyy-MM-ddTHH:mm:ssZ")
                };


                lst.Add(tmp);
            }
            return lst;

        }
    }
}
