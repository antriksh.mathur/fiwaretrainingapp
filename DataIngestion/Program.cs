﻿using DataIngestion.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace DataIngestion
{

    class Program
    {
        static string url = @"https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurants+in+Sydney&key=" + Secret.googleKey;
        static void Main(string[] args)
        {
            //string[] places = new string[] { "kuala lumpur famous places" };
            string[] places = new string[] { "kuala lumpur famous places", "penang famous places", "gurgaon famous places" };

            
            List<PointOfInterest> lstEntities = new List<PointOfInterest>();
            foreach (string place in places)
            {
                var x = GoogleMapsSearch.DoSearch(place, "");
                var y = GoogleMapsSearch.translateToPOI(x);
                lstEntities.AddRange(y);
                
                
            }
            ContextBrokerManager.CreateEntitiesBulk(lstEntities); //also try bulk

            Console.ReadKey();

        }
    }
}

