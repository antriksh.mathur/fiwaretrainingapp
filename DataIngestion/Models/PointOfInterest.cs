﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataIngestion.Models
{

    public class PointOfInterest : IEntity
    {

        public Address address { get; set; }
        public Category category { get; set; }
        public Description description { get; set; }
        public LocationF location { get; set; }
        public Name name { get; set; }
        //public Refseealso refSeeAlso { get; set; }
        public Source source { get; set; }
        public Icon icon { get; set; }
        public string id { get; set; }
        public string type { get; set; }

        public Temperature temperature { get; set; }

        public InceptionDate inceptionDate { get; set; }
    }
    public class InceptionDate
    {
        public string type { get; set; }
        public string value { get; set; }
    }

    public class LocationF
    {
        public string type { get; set; }
        public string value { get; set; }

        public Metadata metadata { get; set; }
    }
    public class Address
    {
        public string type { get; set; }
        public Value value { get; set; }
    }

    public class Metadata
    {
        public Accuracy accuracy { get; set; }        
    }

    public class Accuracy
    {
        public string type { get; set; }
        public int value { get; set; }
    }
    public class Temperature
    {
        public string type { get; set; }
        public int value { get; set; }
    }

    public class Value
    {
        public string addressCountry { get; set; }
        public string addressLocality { get; set; }
        public string streetAddress { get; set; }
    }

    public class Category
    {
        public string[] value { get; set; }
    }

    public class Description
    {
        public string value { get; set; }
    }


    public class Name
    {
        public string value { get; set; }
    }
    public class Refseealso
    {
        public string[] value { get; set; }
    }

    public class Source
    {
        public string value { get; set; }
    }

    public class Icon
    {
        public string value { get; set; }
    }

}
