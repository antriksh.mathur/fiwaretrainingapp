﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataIngestion.Models
{
    interface IEntity
    {
        string id { get; set; }
        string type { get; set; }
    }
}
