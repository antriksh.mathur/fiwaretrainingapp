﻿// Learn more about F# at http://fsharp.org
open System.Runtime.Serialization.Json
open FSharp.Data
open System.Net.Http
open System.Text
open System.Diagnostics
open System.ComponentModel.DataAnnotations
open Newtonsoft.Json
open Newtonsoft.Json.Linq
open System.IO
open System.Diagnostics
open System.Text.RegularExpressions
open System.Net
open System.Linq
open System.Net.Mail

open Secret
open Settings
open System

//let reader =  new JsonTextReader((new DirectoryInfo(Directory.GetCurrentDirectory())).Parent.Parent.Parent.FullName + @"/SubscriptionSettings.json" |> File.OpenText )
//let json = reader |> JToken.ReadFrom |> resolvejsonpath



let sendSms (number:string) (key:string) (sender:string) (message: string) = async {
    if (enablesms = false) then return ()
    let client = new  HttpClient()
    let urlget = sprintf "https://api.textlocal.in/send/?apikey=%s&numbers=%s&message=%s&sender=%s" key number message sender        
    let! getTask = client.GetAsync(urlget) |> Async.AwaitTask
    return getTask       
}


type HttpListener with
    static member Run (url:string,handler: (HttpListenerRequest -> HttpListenerResponse -> Async<unit>)) = 
        let listener = new HttpListener()
        listener.Prefixes.Add url
        listener.Start()
        let asynctask = Async.FromBeginEnd(listener.BeginGetContext,listener.EndGetContext)
        async {

            while true do 
                let! context = asynctask
                Async.Start (handler context.Request context.Response)
        } |> Async.Start 
        listener




let sendMailMessage topic msg =
    let msg = 
        new MailMessage(
            emailfrom, emailto, topic, "Dear " + emailnamefrom + ", <br/><br/>\r\n\r\n" + msg)
    msg.IsBodyHtml <- true
    let client = new SmtpClient(smtpserver, smtpport)
    client.EnableSsl <- true
    client.Credentials <- System.Net.NetworkCredential(emailfrom, password)
    
    client.SendCompleted |> Observable.add(fun e -> 
        let msg = e.UserState :?> MailMessage
        if e.Cancelled then
            ("Mail message cancelled:\r\n" + msg.Subject) |> Console.WriteLine
        if e.Error <> null then
            ("Sending mail failed for message:\r\n" + msg.Subject + 
                ", reason:\r\n" + e.Error.ToString()) |> Console.WriteLine
        if msg<>Unchecked.defaultof<MailMessage> then msg.Dispose()
        if client<>Unchecked.defaultof<SmtpClient> then client.Dispose()
    )    
    client.SendAsync(msg, msg)




HttpListener.Run(listenurl,(fun req resp -> 
        async {            
           
            let stream = req.InputStream
            let reader = new System.IO.StreamReader(stream, req.ContentEncoding)
            let res = reader.ReadToEnd()
            Console.WriteLine res
            let obj = res |> JObject.Parse
            let fin = obj.["data"] |> List.ofSeq |> List.map(fun x -> sprintf "id: %s temperature: %s\n" (x.["id"] |> string)  (x.["temperature"].["value"].ToString(Formatting.None) )) |> Array.ofList |> String.Concat

            
            let! respsms = sendSms smsnumber smsapikey smssender fin
            let respemail = sendMailMessage "Entity Alert" fin
            fin |> ignore
            }
    )) |> ignore
Console.Read () |> ignore
    
